# local module
import logging, requests, time, traceback, os
import urllib.parse
# 3rd module
import telegram 
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

class TelegramBot():
    _token = None
    _updater = None
    _dispatcher = None
    
    # command : function_name
    _commands = dict()

    # chatting source : definition of where message come from, now not support 'group'
    # *** THIS BOT's COMMAND CAT NOT USE IN CHANNEL
    SOURCE = dict(
        chat='chat',
        channel='channel',
    )

    def __init__(self, token : str):
        self._token = token
        self._updater = Updater(token=token, use_context=True)
        self._dispatcher = self._updater.dispatcher

    def run(self):
        self._updater.start_polling()

    @staticmethod
    def resolve_chat_id(update : 'Telegram Update obj') -> '(chat_id : int, source : str)':
        '''
        Resolve the chat_id from Telegram Update obj for reply

        Following source can be resoved:
        1. directly talk        
        2. telegram channel                   
        '''
        if not isinstance(update, telegram.update.Update): raise Exception('Type error')

        if update.message:
            return (update.message.chat_id, TelegramBot.SOURCE['chat'])

        if update.channel_post:
            return (update.channel_post.chat.id, TelegramBot.SOURCE['channel'])

        return None

    @property
    def help_text(self) -> 'str':
        help_text = ['You can use following command to interact with me\n']
        for cmd, hook in self._commands.items():
            help_text.append(f'/{cmd} : {hook.__doc__}')
        return '\n'.join(help_text)

class ServerAliveBot(TelegramBot):
    listen_interval = 600
    protocol = 'http'
    listen_host = 'localhost'
    listen_path = '/'
    # chat rooms bot will inform actively
    active_chat_ids = list()
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._commands['startInform'] = self._get_start_inform_handler()
        self._commands['setHost'] = self._get_host_setter()
        self._commands['setPath'] = self._get_path_setter()
        self._commands['setInterval'] = self._get_time_interval_setter()
        self._commands['info'] = self._get_info_handler()

        for cmd, hook in self._commands.items():
            self._dispatcher.add_handler( CommandHandler(cmd, hook) )

        self._dispatcher.add_handler( CommandHandler('help', self._get_help_message()) )
        self._dispatcher.add_handler( MessageHandler(Filters.command, self._get_help_message()) )
        self._dispatcher.add_handler( MessageHandler(Filters.text, self._get_help_message(only_work_in_personal_chat=True)) )

    @property
    def listen_url(self):
        return f'{self.protocol}://{self.listen_host}{self.listen_path}'
        
    def _get_help_message(self, only_work_in_personal_chat=False) -> 'hook function':
        help_text = self.help_text

        def help_message_handler(update, context):
            chat_id, source = TelegramBot.resolve_chat_id(update)

            if only_work_in_personal_chat is True and source != TelegramBot.SOURCE['chat']: 
                return

            context.bot.send_message(chat_id , text=help_text)

        return help_message_handler

    def _get_start_inform_handler(self):
        
        def start_inform_handler(update, context):
            ''' Bot will start inform server health in this chat '''
            chat_id, source = TelegramBot.resolve_chat_id(update)
            if chat_id not in self.active_chat_ids:
                self.active_chat_ids.append(chat_id)

            context.bot.send_message(chat_id , text='收到命令, 我將會定時回報伺服器健康狀態')

        return start_inform_handler

    def _get_host_setter(self):

        def host_setter(update, context):
            ''' "/setHost hostName" will set host to listen '''
            chat_id, source = TelegramBot.resolve_chat_id(update)

            try:
                self.listen_host = str(context.args[0])
                context.bot.send_message(chat_id , text=f'設定傾聽主機為 : {self.listen_host}')
            except Exception as e:
                context.bot.send_message(chat_id , text='設定傾聽主機失敗')

        return host_setter
        
    def _get_path_setter(self):

        def path_setter(update, context):
            ''' "/setHost hostName" will set host to listen '''
            chat_id, source = TelegramBot.resolve_chat_id(update)

            try:
                self.listen_path = str(context.args[0])
                context.bot.send_message(chat_id , text=f'設定傾聽路徑為 : {self.listen_path}')
            except Exception as e:
                context.bot.send_message(chat_id , text='設定傾聽路徑失敗')

        return path_setter

    def _get_time_interval_setter(self):
        
        def time_interval_setter(update, context):
            ''' "/setInterval seconds" to set time-interval (second) bot request server to detect alive '''
            chat_id, source = TelegramBot.resolve_chat_id(update)

            try:
                origin_interval = self.listen_interval
                self.listen_interval = int(context.args[0])
                context.bot.send_message(chat_id , text=f'設定傾聽週期由 {origin_interval} 改為 : {self.listen_interval} 秒\n(已經開始的等待將不會被中斷)')
            except Exception as e:
                context.bot.send_message(chat_id , text='設定傾聽週期失敗')

        return time_interval_setter

    def _get_info_handler(self):

        def info_handler(update, context):
            ''' Show setting of this bot '''
            chat_id, source = TelegramBot.resolve_chat_id(update)
            is_routine_report = chat_id in self.active_chat_ids

            tmp = [
                f'您好，ServerAliveBot 目前的設定為\n',
                f'1. 定期回報: \t{is_routine_report}',
                f'2. 傾聽週期: \t{self.listen_interval} 秒',
                f'3. 傾聽服務: \t{self.protocol}://{self.listen_host}{self.listen_path}'
            ]
            tmp = '\n'.join(tmp)
            context.bot.send_message(chat_id , text=tmp)

        return info_handler

    def run(self):
        super().run()

        # user TREAD is better
        while True:
            print(f'Wait for {self.listen_interval} sec')
            time.sleep(self.listen_interval)
            
            error = '無'
            comment = list([f'傾聽服務 : \t{self.listen_url}'])
            try:
                res = requests.get(self.listen_url)
                comment.append(f'status code : \t{res.status_code}') 
            except Exception as e:
                traceback.print_exc()
                error = e.__str__()
                comment.append(f'錯誤 : \t{error}') 

            comment = '\n'.join(comment)
            query_string_comment = urllib.parse.quote(comment)
            for chat_id in self.active_chat_ids:
                reply = requests.get(f'https://api.telegram.org/bot{self._token}/sendMessage?chat_id={chat_id}&text={query_string_comment}')
                logging.info(f'Inform {chat_id}, status_code: {reply.status_code}')

if __name__ == '__main__':
    bot = ServerAliveBot(token=os.getenv('TELEGRAM_TOKEN') or '843379622:AAEW2ErPrFOzYZTG4to0fMQfdupt6scZjwc')
    bot.run()
